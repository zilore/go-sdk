# Zilore SDK

See https://zilore.com/en/help/api for more information.<br/>
Go to https://my.zilore.com/account/api for the authorization key.

### Installation 

```
go get -u gitlab.com/zilore/go-sdk/zilore
```

### Example

```go
package main

import "gitlab.com/zilore/go-sdk/zilore"

func main() {
	client := zilore.New(&zilore.Config{
		AuthProvider: zilore.NewAuthKeyProvider("6fac94db-691b-ec08-d22c-00000b7c06c0"),
	})
	err := client.DNS().Domain("example.com").AddRecord(&zilore.Record{
		Name:  "foo.example.com",
		Type:  zilore.RecordTypeCNAME,
		TTL:   3600,
		Value: "boo.example.com",
	})
	if err != nil {
		// ...
	}
}

```