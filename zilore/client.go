package zilore

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/google/go-querystring/query"
)

// Client is a Zilore client.
type Client struct {
	client *http.Client
}

// New creates new Zilore client.
// See https://zilore.com/en/help/api for more information.
// Go to https://my.zilore.com/account/api for the authorization key.
func New(config *Config) *Client {
	return &Client{
		client: &http.Client{
			Timeout:   config.getTimeout(),
			Transport: config.getTransport(),
		},
	}
}

// Monitoring returns monitoring service functions.
func (c *Client) Monitoring() *Monitoring { return &Monitoring{client: c} }

// DNS returns domain name service function.
func (c *Client) DNS() *DNS { return &DNS{client: c} }

func (c *Client) Do(method, uri string, params any, result any) error {
	req, err := c.NewRequest(method, uri, params)
	if err != nil {
		return err
	}
	res, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	return c.ParseResult(res, result)
}

func (c *Client) ParseResult(res *http.Response, result any) error {
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}
	r := &Response{Response: result}
	err = json.Unmarshal(body, r)
	if r.Error != nil {
		return r.Error
	}
	if res.StatusCode/100 != 2 {
		return fmt.Errorf("bad status code: %d", res.StatusCode)
	}
	if err != nil {
		return err
	}
	if r.Status != StatusOK {
		return fmt.Errorf("bad response status: %s", r.Status)
	}
	return nil
}

func (c *Client) NewRequest(method, uri string, params any) (*http.Request, error) {
	base := &url.URL{Scheme: "https", Host: "api.zilore.com"}
	ref, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}
	u := base.ResolveReference(ref)
	q, err := query.Values(params)
	if err != nil {
		return nil, err
	}
	var body io.Reader
	if strings.EqualFold(http.MethodPost, method) {
		body = strings.NewReader(q.Encode())
	} else {
		u.RawQuery = q.Encode()
	}
	req, err := http.NewRequest(method, u.String(), body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", "")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json")
	return req, nil
}

const (
	StatusOK   = "ok"
	StatusFail = "fail"
)

type Response struct {
	Status   string `json:"status"`
	Response any    `json:"response"`
	Error    *Error `json:"error"`
	Details  any    `json:"details"`
}

type Error struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func (e *Error) Error() string { return e.Message }
