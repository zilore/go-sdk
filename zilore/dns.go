package zilore

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
)

type DNS struct {
	client *Client
}

func (c *DNS) ListDomains(opt *ListOptions) ([]*Domain, error) {
	var domains []*Domain
	err := c.client.Do(http.MethodGet, "/dns/v1/domains", opt, &domains)
	if err != nil {
		return nil, err
	}
	return domains, nil
}

func (c *DNS) ListTopLevelDomains() ([]string, error) {
	res := struct {
		Domains []string `json:"tlds"`
	}{}
	err := c.client.Do(http.MethodGet, "/dns/v1/tlds", nil, &res)
	if err != nil {
		return nil, err
	}
	return res.Domains, nil
}

func (c *DNS) Domain(domain string) *DomainService {
	return &DomainService{client: c.client, domain: domain}
}

func (c *DNS) AddDomain(domain string) error {
	opt := struct {
		Domain string `url:"domain_name"`
	}{Domain: domain}
	err := c.client.Do(http.MethodPost, "/dns/v1/domains", opt, nil)
	return err
}

func (c *DNS) DeleteDomain(domain string) error {
	opt := struct {
		Domain string `url:"domain_name"`
	}{Domain: domain}
	err := c.client.Do(http.MethodDelete, "/dns/v1/domains", opt, nil)
	return err
}

type ListOptions struct {
	Offset  int    `url:"offset,omitempty"`
	Limit   int    `url:"limit,omitempty"`
	OrderBy string `url:"order_by,omitempty"`
	Order   string `url:"order_param,omitempty"`
	Search  string `url:"search_text,omitempty"`
}

const (
	OrderAsc  = "asc"
	OrderDesc = "desc"
)

const (
	DomainStatusNoNameServers    = "NONE"
	DomainStatusOtherNameServers = "OTHER"
	DomainStatusDelegated        = "DELEGATED"
	DomainStatusDelegatedPartial = "PARTIAL"
	DomainStatusUnknown          = "UNKNOWN"
)

type Domain struct {
	ID     ID     `json:"domain_id"`
	Status string `json:"domain_status"`
	Name   string `json:"domain_name"`
}

const (
	PeriodToday     = "today"
	PeriodWeek      = "week"
	PeriodMonth     = "month"
	PeriodLastMonth = "last_month"
)

type DomainStatOptions struct {
	Period string `url:"period,omitempty"`
}

type DomainStat struct {
	Presets   DomainStatPresets `json:"presets"`
	Today     StatData          `json:"today"`
	Week      StatData          `json:"week"`
	Month     StatData          `json:"month"`
	LastMonth StatData          `json:"last_month"`
}

type DomainStatPresets struct {
	Today     int64 `json:"today"`
	Week      int64 `json:"week"`
	Month     int64 `json:"month"`
	LastMonth int64 `json:"last_month"`
}

type StatData map[string]int64

func (data *StatData) UnmarshalJSON(b []byte) error {
	m := make(map[string]string)
	err := json.Unmarshal(b, &m)
	if err != nil {
		return err
	}
	r := make(map[string]int64)
	for k, v := range m {
		count, err := strconv.ParseInt(v, 10, 64)
		if err != nil {
			return err
		}
		r[k] = count
	}
	*data = r
	return nil
}

type ID string

func (data *ID) UnmarshalJSON(b []byte) error {
	if len(b) == 0 {
		return io.ErrUnexpectedEOF
	}
	if b[0] == '"' {
		var s string
		if err := json.Unmarshal(b, &s); err != nil {
			return err
		}
		*data = ID(s)
		return nil
	}
	var v int64
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	*data = ID(strconv.FormatInt(v, 10))
	return nil
}
