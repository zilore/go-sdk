package zilore

import "net/http"

type AuthProvider http.RoundTripper

// NewAuthKeyProvider returns key auth provider.
func NewAuthKeyProvider(key string) AuthProvider {
	return NewAuthKeyProviderWithTransport(key, http.DefaultTransport)
}

// NewAuthKeyProviderWithTransport returns key auth provider with c.
func NewAuthKeyProviderWithTransport(key string, tr http.RoundTripper) AuthProvider {
	return &authKeyProvider{tr, key}
}

type authKeyProvider struct {
	inner http.RoundTripper
	key   string
}

func (p *authKeyProvider) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("X-Auth-Key", p.key)
	return p.inner.RoundTrip(req)
}
