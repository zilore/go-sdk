package zilore

import (
	"net/http"
	"time"
)

// Config is a Zilore client config.
type Config struct {
	Timeout      time.Duration
	AuthProvider AuthProvider
}

func (c *Config) getTimeout() time.Duration {
	if c.Timeout != 0 {
		return c.Timeout
	}
	return 10 * time.Second
}

func (c *Config) getTransport() http.RoundTripper {
	if c.AuthProvider != nil {
		return c.AuthProvider
	}
	return http.DefaultTransport
}
