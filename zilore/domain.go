package zilore

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"
)

type DomainService struct {
	client *Client
	domain string
}

func (c *DomainService) Nameservers() ([]string, error) {
	res := struct {
		Nameservers string `json:"nameservers"`
	}{}
	uri := path.Join("/dns/v1/domains", url.PathEscape(c.domain), "nameservers")
	err := c.client.Do(http.MethodGet, uri, nil, &res)
	if err != nil {
		return nil, err
	}
	return strings.Split(res.Nameservers, ","), nil
}

func (c *DomainService) Stats(opt *DomainStatOptions) (*DomainStat, error) {
	stat := &DomainStat{}
	uri := path.Join("/dns/v1/domains", url.PathEscape(c.domain), "statistics")
	err := c.client.Do(http.MethodGet, uri, opt, stat)
	if err != nil {
		return nil, err
	}
	return stat, nil
}

func (c *DomainService) ListRecords(opt *ListOptions) ([]*Record, error) {
	var records []*Record
	uri := path.Join("/dns/v1/domains", url.PathEscape(c.domain), "records")
	err := c.client.Do(http.MethodGet, uri, opt, &records)
	if err != nil {
		return nil, err
	}
	return records, nil
}

func (c *DomainService) AddRecord(record *Record) error {
	opt := RecordOptions{
		Name:  record.Name,
		Type:  record.Type,
		Value: record.Value,
		TTL:   record.TTL,
	}
	res := struct {
		ID ID `json:"record_id"`
	}{}
	uri := path.Join("/dns/v1/domains", url.PathEscape(c.domain), "records")
	err := c.client.Do(http.MethodPost, uri, opt, &res)
	if err != nil {
		return err
	}
	record.ID = res.ID
	record.Status = RecordStatusEnabled
	return nil
}

func (c *DomainService) UpdateRecord(record *Record) error {
	opt := RecordOptions{
		Name:  record.Name,
		Type:  record.Type,
		Value: record.Value,
		TTL:   record.TTL,
	}
	uri := path.Join("/dns/v1/domains", url.PathEscape(c.domain), "records", url.PathEscape(string(record.ID)))
	err := c.client.Do(http.MethodPut, uri, opt, nil)
	if err != nil {
		return err
	}
	return nil
}

func (c *DomainService) UpdateRecordStatus(record *Record) error {
	opt := struct {
		Status string `url:"record_status"`
	}{Status: record.Status}
	uri := path.Join("/dns/v1/domains", url.PathEscape(c.domain), "records", url.PathEscape(string(record.ID)), "status")
	err := c.client.Do(http.MethodPut, uri, opt, nil)
	return err
}

func (c *DomainService) DeleteRecord(id ID) error {
	opt := struct {
		ID ID `url:"record_id"`
	}{ID: id}
	uri := path.Join("/dns/v1/domains", url.PathEscape(c.domain), "records")
	err := c.client.Do(http.MethodDelete, uri, opt, nil)
	return err
}

const (
	RecordStatusEnabled  = "enabled"
	RecordStatusDisabled = "disabled"
)

type RecordOptions struct {
	Name  string `url:"record_name"`
	Type  string `url:"record_type"`
	Value string `url:"record_value"`
	TTL   int    `url:"record_ttl"`
}

type Record struct {
	ID         ID     `json:"record_id"`
	Status     string `json:"record_status"`
	Name       string `json:"record_name"`
	Type       string `json:"record_type"`
	Value      string `json:"record_value"`
	TTL        int    `json:"record_ttl,string"`
	IsGeo      Bool   `json:"is_geo"`
	IsFailover Bool   `json:"is_failover"`
}

type Bool bool

func (data *Bool) UnmarshalJSON(b []byte) error {
	if len(b) == 0 {
		return io.ErrUnexpectedEOF
	}
	if b[0] == '"' {
		var s string
		if err := json.Unmarshal(b, &s); err != nil {
			return err
		}
		switch strings.ToLower(s) {
		case "no":
			*data = false
		case "yes":
			*data = false
		default:
			b, err := strconv.ParseBool(s)
			if err != nil {
				return err
			}
			*data = Bool(b)
		}
		return nil
	}
	var v bool
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	*data = Bool(v)
	return nil
}

const (
	RecordTypeA     = "A"
	RecordTypeAAAA  = "AAAA"
	RecordTypeCNAME = "CNAME"
	RecordTypeALIAS = "ALIAS"
	RecordTypeMX    = "MX"
	RecordTypeTXT   = "TXT"
	RecordTypeNS    = "NS"
	RecordTypeSRV   = "SRV"
	RecordTypePTR   = "PTR"
	RecordTypeCAA   = "CAA"
	RecordTypeSOA   = "SOA"
)
