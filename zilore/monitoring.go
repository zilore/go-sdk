package zilore

import "net/http"

type Monitoring struct {
	client *Client
}

func (c *Monitoring) ListServers() ([]*MonitoringServer, error) {
	var servers []*MonitoringServer
	err := c.client.Do(http.MethodGet, "monitoring/v1/list_servers", nil, &servers)
	if err != nil {
		return nil, err
	}
	return servers, nil
}

type MonitoringServer struct {
	IP       string `json:"checker_ip"`
	Hostname string `json:"checker_hostname"`
	Name     string `json:"checker_name"`
}
